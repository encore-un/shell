#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <stdlib.h>

void  ChildProcess(int);                /* child process prototype  */
void  ParentProcess(int);               /* parent process prototype */

void  main(void)
{
    pause();
}
